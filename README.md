# Prodyna FE dev test

This is a simple web app project in [React.js](https://reactjs.org/) bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/).

## Technical spec

[Node.js](https://nodejs.org/) version used `v16.3.2`

[Yarn](https://yarnpkg.com/) version used `1.22.17`

Main task was to create app for fetching, creating and searching through posts using [{JSON} Placeholder](https://jsonplaceholder.typicode.com/) mockup service.
[Axios](https://axios-http.com) was used for HTTP request as small but powerful library.
Infinite scroll was implemented using [Intersection Observer API](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API).

## Getting Started DEV

First, install `node_modules`:

```bash
yarn
```

Then, run the development server:

```bash
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Getting Started PROD

First, install `node_modules`:

```bash
yarn
```

Then, run the build project:

```bash
yarn build
```

## Styling

[Sass](https://sass-lang.com/documentation) was used for styling, specifically [Sass modules](https://create-react-app.dev/docs/adding-a-css-modules-stylesheet).

## Clean Code

Keep code clean with [ESlint](https:/eslint.org/) and [Prettier](https://prettier.io/).

To format code on demand:

```bash
yarn format
```

## Useful Links

To learn more about technologies used take a look at the following resources:

-   [React.js Documentation](https://reactjs.org/docs/getting-started.html)
-   [Redux.js Documentation](https://redux.js.org/introduction/getting-started)
-   [Redux Toolkit Documentation](https://redux-toolkit.js.org/introduction/getting-started)
-   [React Router Documentation](https://reactrouterdotcom.fly.dev/docs/en/v6)
-   [Axios Documentation](https://axios-http.com/docs/intro)
-   [Lodash Documentation](https://lodash.com/docs/4.17.15)
