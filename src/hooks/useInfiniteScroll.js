import { useCallback, useRef, useState } from 'react';

import { INITIAL_PAGE, STATUS } from 'util/constants/defaultValues';

const useInfiniteScroll = (postsStatus, hasMorePosts) => {
    const [page, setPage] = useState(INITIAL_PAGE);
    const observer = useRef();

    const lastElementRef = useCallback(
        (node) => {
            if (postsStatus === STATUS.LOADING) {
                return;
            }

            if (observer.current) {
                observer.current.disconnect();
            }

            observer.current = new IntersectionObserver((entries) => {
                if (entries[0].isIntersecting && hasMorePosts) {
                    setPage((prev) => prev + 1);
                }
            });

            if (node) {
                observer.current.observe(node);
            }
        },
        [postsStatus, hasMorePosts]
    );

    return [lastElementRef, page, setPage];
};

export { useInfiniteScroll };
