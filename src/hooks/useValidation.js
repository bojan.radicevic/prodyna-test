import { useState } from 'react';
import { ERROR_MESSAGES } from 'util/constants/defaultValues';

const useValidation = (input, err) => {
    const [errors, setErrors] = useState(err);

    const validate = () => {
        const newInput = { ...input };
        const newErrors = { ...errors };
        let isValid = true;

        Object.keys(newInput).forEach((key) => {
            newInput[key] = newInput[key].trim();
            if (!newInput[key]) {
                isValid = false;
                newErrors[key] = ERROR_MESSAGES.REQUIRED_FIELD;
            }
        });

        setErrors(newErrors);

        return isValid;
    };

    return [errors, setErrors, validate];
};

export { useValidation };
