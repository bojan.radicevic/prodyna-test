import React, { Suspense, lazy } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import { MAIN_URLS } from 'util/routes/urls';

import App from 'App';
// import Posts from 'pages/posts/Posts';
import Loader from 'components/loader/Loader';

const CreatePost = lazy(() => import('pages/createPost/CreatePost'));
const Posts = lazy(() => import('pages/posts/Posts'));

const Router = () => {
	return (
		<Suspense fallback={<Loader />}>
			<Routes>
				<Route element={<App />}>
					<Route path={MAIN_URLS.POSTS} element={<Posts />} />
					<Route
						path={MAIN_URLS.CREATE_POST}
						element={<CreatePost />}
					/>
				</Route>
				<Route
					path={MAIN_URLS.ROOT}
					element={<Navigate to={MAIN_URLS.POSTS} />}
				/>
				<Route path="*" element={<Navigate to={MAIN_URLS.POSTS} />} />
			</Routes>
		</Suspense>
	);
};

export default Router;
