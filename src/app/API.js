import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        'Content-Type': 'application/json; charset=utf-8',
    },
});

export default axiosInstance;
