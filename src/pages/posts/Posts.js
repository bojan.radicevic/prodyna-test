import React, { useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import debounce from 'lodash.debounce';

import { useInfiniteScroll } from 'hooks/useInfiniteScroll';

import {
    fetchPostsAction,
    deletePostAction,
    searchPostsAction,
} from 'store/posts/postsSlice';
import {
    selectPosts,
    selectPostsStatus,
    selectHasMorePosts,
} from 'store/posts/postSelector';
import { INITIAL_PAGE, PAGE_LIMIT, STATUS } from 'util/constants/defaultValues';

import PostCard from './postCard/PostCard';
import Loader from 'components/loader/Loader';
import Search from 'components/search/Search';

const Posts = () => {
    const hasMorePosts = useSelector(selectHasMorePosts);
    const posts = useSelector(selectPosts);
    const postsStatus = useSelector(selectPostsStatus);
    const [searchParams, setSearchParams] = useSearchParams();
    const [query, setQuery] = useState(searchParams.get('q') || '');
    const [lastElementRef, page, setPage] = useInfiniteScroll(
        postsStatus,
        hasMorePosts
    );
    const dispatch = useDispatch();

    const searchChangeHandler = ({ target }) => {
        setPage(INITIAL_PAGE);
        setQuery(target.value);
        setSearchParams({ q: target.value.trim() });
    };

    const deletePost = (postId) => {
        dispatch(deletePostAction(postId));
    };

    const searchHandler = (query, page) => {
        dispatch(searchPostsAction({ query, page, limit: PAGE_LIMIT }));
    };

    const searchHandlerDebounce = useCallback(debounce(searchHandler, 500), []);

    useEffect(() => {
        query
            ? searchHandlerDebounce(query, page)
            : dispatch(fetchPostsAction({ page, limit: PAGE_LIMIT }));

        return () => {
            searchHandlerDebounce.cancel();
        };
    }, [page, query]);

    return (
        <>
            <Search
                value={query}
                onChange={searchChangeHandler}
                placeholder="Search posts"
                noResults={query && posts?.length === 0}
            />
            {posts?.map((post, index) => {
                return (
                    <PostCard
                        key={post?.id}
                        {...post}
                        ref={posts.length === index + 1 ? lastElementRef : null}
                        deletePost={deletePost}
                        disabled={postsStatus === STATUS.LOADING}
                    >
                        {post?.body}
                    </PostCard>
                );
            })}
            {postsStatus === STATUS.LOADING && <Loader />}
        </>
    );
};

export default Posts;
