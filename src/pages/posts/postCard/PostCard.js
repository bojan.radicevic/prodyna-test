import { useState, useEffect, useRef, useCallback, forwardRef } from 'react';
import debounce from 'lodash.debounce';

import { DEFAULT_HEIGHT } from 'util/constants/defaultValues';

import Avatar from 'components/avatar/Avatar';
import DismissButton from 'components/dismissButton/DismissButton';

import { capitalizeFirstLetter } from 'util/helpers/capitalize';

import styles from './PostCard.module.scss';

const PostCard = forwardRef(
    ({ title, children, user, disabled, deletePost, id }, ref) => {
        const [height, setHeight] = useState(DEFAULT_HEIGHT);
        const [isActive, setIsActive] = useState(false);
        const contentRef = useRef();

        const calculateHeight = () => {
            setHeight(contentRef.current.scrollHeight);
        };

        const calculateHeightDebounce = useCallback(
            debounce(calculateHeight, 200),
            []
        );

        useEffect(() => {
            // Dynamically calculating height of contentRef element
            calculateHeight();
            // Also on resize
            window.addEventListener('resize', calculateHeightDebounce);

            return () => {
                calculateHeightDebounce.cancel();
                window.removeEventListener('resize', calculateHeightDebounce);
            };
        }, []);

        return (
            <div className={styles.postcard} aria-expanded={isActive} ref={ref}>
                <DismissButton
                    onClick={() => deletePost(id)}
                    disabled={disabled}
                    style={{ position: 'absolute', top: '8px', right: '8px' }}
                />
                <div
                    className={styles.postcard__label}
                    onClick={() => setIsActive((prevState) => !prevState)}
                >
                    <div className={styles.postscard__avatar}>
                        <Avatar src={user?.avatarSrc} />
                        <span>{user?.name}</span>
                    </div>
                    <div>{title && capitalizeFirstLetter(title)}</div>
                </div>
                <div
                    className={styles.postcard__inner}
                    style={{
                        height: `${isActive ? height : DEFAULT_HEIGHT}px`,
                    }}
                    aria-hidden={!isActive}
                >
                    <div className={styles.postcard__content} ref={contentRef}>
                        {children && capitalizeFirstLetter(children)}
                    </div>
                </div>
            </div>
        );
    }
);

export default PostCard;
