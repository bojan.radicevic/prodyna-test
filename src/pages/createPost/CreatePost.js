import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { createPostAction, resetPostCreated } from 'store/posts/postsSlice';
import { selectPostCreated, selectPostsStatus } from 'store/posts/postSelector';

import { MAIN_URLS } from 'util/routes/urls';
import { STATUS } from 'util/constants/defaultValues';
import { useValidation } from 'hooks/useValidation';

import Input from 'components/input/Input';
import Loader from 'components/loader/Loader';
import ResizableTextarea from 'components/resizableTextarea/ResizableTextarea';

import styles from './CreatePost.module.scss';

const initialState = { title: '', body: '' };

const CreatePost = () => {
    const isPostCreated = useSelector(selectPostCreated);
    const postsStatus = useSelector(selectPostsStatus);
    const [input, setInput] = useState(initialState);
    const [errors, setErrors, checkIsValid] = useValidation(
        input,
        initialState
    );
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const inputRef = useRef();

    useLayoutEffect(() => {
        inputRef.current.focus();
    }, []);

    useEffect(() => {
        // isPostCreated is a flag for redirect
        if (isPostCreated) {
            // Reset redirect flag (isPostCreated)
            dispatch(resetPostCreated());
            navigate(MAIN_URLS.POSTS, { replace: true });
        }
    }, [isPostCreated]);

    const onChangeHandler = ({ target }) => {
        setInput({ ...input, [target.name]: target.value });
        if (errors[target.name]) {
            setErrors({ ...errors, [target.name]: '' });
        }
    };

    const onSubmitHandler = (event) => {
        event.preventDefault();

        if (checkIsValid()) {
            dispatch(createPostAction(input));
        }
    };

    return (
        <>
            <div className={styles.create_post}>
                <form onSubmit={onSubmitHandler}>
                    <Input
                        name="title"
                        value={input?.title}
                        onChange={onChangeHandler}
                        placeholder="Title"
                        label="Post title:"
                        error={errors?.title}
                        ref={inputRef}
                    />
                    <ResizableTextarea
                        name="body"
                        value={input?.body}
                        onChange={onChangeHandler}
                        placeholder="Write something..."
                        label="Post body:"
                        error={errors?.body}
                    />

                    <input
                        type="submit"
                        value="Submit"
                        className={styles.create_post__submit}
                        disabled={postsStatus === STATUS.LOADING}
                    />
                </form>
            </div>
            {postsStatus === STATUS.LOADING && <Loader />}
        </>
    );
};

export default CreatePost;
