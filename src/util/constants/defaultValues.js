export const PAGE_LIMIT = 25;
export const INITIAL_PAGE = 1;
export const DEFAULT_HEIGHT = 0;
export const MIN_TEXTAREA_HEIGHT = 112;
export const STATUS = {
    LOADING: 'loading',
    IDLE: 'idle',
};
export const ERROR_MESSAGES = {
    REQUIRED_FIELD: 'This field is required',
};
export const DEFAULT_AVATAR_IMG_SIZE = {
    WIDTH: 200,
    HEIGHT: 200,
};
export const MOCKUP_AVATAR_BASE_URL = 'https://loremflickr.com';
