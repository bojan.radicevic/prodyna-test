export const MAIN_URLS = {
    ROOT: '/',
    POSTS: '/posts',
    POST: '/post/:id',
    CREATE_POST: '/create-post',
};
