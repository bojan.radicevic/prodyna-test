import { createSelector } from '@reduxjs/toolkit';

const selectPosts = createSelector(
    (state) => state.posts,
    (posts) => posts.data
);

const selectPostsStatus = createSelector(
    (state) => state.posts,
    (posts) => posts.status
);

const selectHasMorePosts = createSelector(
    (state) => state.posts,
    (posts) => posts.hasMorePosts
);

const selectPostCreated = createSelector(
    (state) => state.posts,
    (posts) => posts.postCreated
);

export {
    selectPosts,
    selectPostsStatus,
    selectHasMorePosts,
    selectPostCreated,
};
