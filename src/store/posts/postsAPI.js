import API from 'app/API';

import { API_URLS } from 'util/routes/apiUrls';

export const fetchPosts = (page, limit) =>
    API.get(`${API_URLS.POSTS}?_expand=user&_page=${page}&_limit=${limit}`);

export const deletePost = (postId) => API.delete(`${API_URLS.POST}${postId}`);

export const createPost = (payload) => API.post(`${API_URLS.POSTS}`, payload);

export const searchPosts = (query, page, limit) =>
    API.get(
        `${API_URLS.POSTS}?q=${query}&_expand=user&_page=${page}&_limit=${limit}`
    );
