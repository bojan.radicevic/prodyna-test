import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
	DEFAULT_AVATAR_IMG_SIZE,
	MOCKUP_AVATAR_BASE_URL,
	STATUS,
} from 'util/constants/defaultValues';
import { deletePost, fetchPosts, createPost, searchPosts } from './postsAPI';

const initialState = {
	data: [],
	addedData: [],
	status: STATUS.IDLE,
	hasMorePosts: false,
	postCreated: false,
};

export const fetchPostsAction = createAsyncThunk(
	'posts/fetchPosts',
	async ({ page, limit }) => {
		const response = await fetchPosts(page, limit);
		response?.data?.forEach((post) => {
			post.user.avatarSrc = `
				${MOCKUP_AVATAR_BASE_URL}
				/${DEFAULT_AVATAR_IMG_SIZE.WIDTH}
				/${DEFAULT_AVATAR_IMG_SIZE.HEIGHT}
				?random=${post?.user?.id}
			`;
		});
		return { page, posts: response.data };
	}
);

export const deletePostAction = createAsyncThunk(
	'posts/deletePost',
	async (postId) => {
		await deletePost(postId);
		return postId;
	}
);

export const createPostAction = createAsyncThunk(
	'posts/createPost',
	async (payload) => {
		const newPayload = { ...payload };
		Object.keys(newPayload).forEach(
			(key) => (newPayload[key] = newPayload[key].trim())
		);
		const response = await createPost(newPayload);
		response.data.user = { name: 'Anonymous' };
		response.data.id = Date.now();
		return response.data;
	}
);

export const searchPostsAction = createAsyncThunk(
	'posts/searchPosts',
	async ({ query, page, limit }) => {
		const response = await searchPosts(query.trim(), page, limit);
		response?.data?.forEach((post) => {
			post.user.avatarSrc = `
				${MOCKUP_AVATAR_BASE_URL}
				/${DEFAULT_AVATAR_IMG_SIZE.WIDTH}
				/${DEFAULT_AVATAR_IMG_SIZE.HEIGHT}
				?random=${post?.user?.id}
			`;
		});
		return { query: query.trim(), page, posts: response.data };
	}
);

export const postsSlice = createSlice({
	name: 'posts',
	initialState,
	reducers: {
		resetPostCreated: (state) => {
			state.postCreated = false;
		},
	},
	extraReducers: (builder) => {
		// Fetch posts
		builder
			.addCase(fetchPostsAction.pending, (state) => {
				state.status = STATUS.LOADING;
			})
			.addCase(fetchPostsAction.fulfilled, (state, action) => {
				const { page, posts } = action?.payload;
				state.hasMorePosts = posts.length !== 0;
				if (page === 1) {
					state.data = [...state.addedData, ...posts];
				} else {
					state.data = [...state.data, ...posts];
				}
				state.status = STATUS.IDLE;
			})
			.addCase(fetchPostsAction.rejected, (state) => {
				state.status = STATUS.IDLE;
			});
		// Delete post
		builder
			.addCase(deletePostAction.pending, (state) => {
				state.status = STATUS.LOADING;
			})
			.addCase(deletePostAction.fulfilled, (state, action) => {
				state.data = state.data.filter(
					(post) => post.id !== action.payload
				);
				state.addedData = state.addedData.filter(
					(post) => post.id !== action.payload
				);
				state.status = STATUS.IDLE;
			})
			.addCase(deletePostAction.rejected, (state) => {
				state.status = STATUS.IDLE;
			});
		// Create post
		builder
			.addCase(createPostAction.pending, (state) => {
				state.status = STATUS.LOADING;
			})
			.addCase(createPostAction.fulfilled, (state, action) => {
				state.data = [action?.payload, ...state?.data];
				state.addedData = [action?.payload, ...state.addedData];
				state.postCreated = true;
				state.status = STATUS.IDLE;
			})
			.addCase(createPostAction.rejected, (state) => {
				state.status = STATUS.IDLE;
			});
		// Search posts
		builder
			.addCase(searchPostsAction.pending, (state) => {
				state.status = STATUS.LOADING;
			})
			.addCase(searchPostsAction.fulfilled, (state, action) => {
				const { query, page, posts } = action?.payload;
				const fileteredAddedData = state.addedData.filter((obj) =>
					Object.values(obj).some((val) =>
						typeof val === 'string'
							? val.toLowerCase().includes(query.toLowerCase())
							: null
					)
				);
				state.hasMorePosts = posts.length !== 0;
				if (page === 1) {
					state.data = [...fileteredAddedData, ...posts];
				} else {
					state.data = [...state.data, ...posts];
				}
				state.status = STATUS.IDLE;
			})
			.addCase(searchPostsAction.rejected, (state) => {
				state.status = STATUS.IDLE;
			});
	},
});

export const { resetPostCreated, clearPosts, clearSearch } = postsSlice.actions;

export default postsSlice.reducer;
