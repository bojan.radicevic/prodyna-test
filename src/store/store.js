import { configureStore } from '@reduxjs/toolkit';
import postsReducer from 'store/posts/postsSlice';

export const store = configureStore({
    reducer: {
        posts: postsReducer,
    },
});
