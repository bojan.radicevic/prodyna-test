import React, { forwardRef } from 'react';

import styles from './Input.module.scss';

const Input = forwardRef(
    ({ name, label, onChange, value, placeholder, error }, ref) => {
        return (
            <div className={styles.input_wrapper}>
                <label htmlFor={name} className={styles.input__label}>
                    {label}
                </label>
                <input
                    type="text"
                    className={styles.input}
                    id={name}
                    name={name}
                    onChange={onChange}
                    ref={ref}
                    value={value}
                    placeholder={placeholder}
                />
                <div className={styles.input__error}>{error}</div>
            </div>
        );
    }
);

export default Input;
