import React, { useLayoutEffect, useRef } from 'react';

import { MIN_TEXTAREA_HEIGHT } from 'util/constants/defaultValues';

import styles from './ResizableTextarea.module.scss';

const ResizableTextarea = ({
    name,
    label,
    onChange,
    value,
    placeholder,
    error,
}) => {
    const textareaRef = useRef();

    useLayoutEffect(() => {
        // Reset height - important to shrink on delete
        textareaRef.current.style.height = 'inherit';
        // Set height
        textareaRef.current.style.height = `${Math.max(
            textareaRef.current.scrollHeight,
            MIN_TEXTAREA_HEIGHT
        )}px`;
    }, [value]);

    return (
        <div className={styles.resizable_textarea_wrapper}>
            <label htmlFor={name} className={styles.resizable_textarea__label}>
                {label}
            </label>
            <textarea
                className={styles.resizable_textarea}
                id={name}
                name={name}
                onChange={onChange}
                ref={textareaRef}
                value={value}
                placeholder={placeholder}
                style={{
                    minHeight: MIN_TEXTAREA_HEIGHT,
                }}
            />
            <div className={styles.resizable_textarea__error}>{error}</div>
        </div>
    );
};

export default ResizableTextarea;
