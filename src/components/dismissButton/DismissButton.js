import React from 'react';

import { ReactComponent as Delete } from 'assets/icons/delete_icon.svg';

import styles from './DismissButton.module.scss';

const Dismissbutton = ({ disabled, onClick, style }) => {
	return (
		<button
			aria-label="Dissmis"
			className={styles.delete_button}
			onClick={onClick}
			style={{ ...style }}
			disabled={disabled}
		>
			<Delete />
		</button>
	);
};

export default Dismissbutton;
