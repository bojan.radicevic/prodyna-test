import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import { MAIN_URLS } from 'util/routes/urls';

import DismissButton from 'components/dismissButton/DismissButton';

import logo from 'assets/images/prodyna_logo.png';

import styles from './SideDrawer.module.scss';

const SideDrawer = ({ show, drawerClickHandler }) => {
    return (
        <nav
            className={[styles.side_drawer, show ? styles.open : null].join(
                ' '
            )}
        >
            <header>
                <Link to={MAIN_URLS.POST}>
                    <img
                        src={logo}
                        className={styles.side_drawer__logo}
                        alt="logo"
                    />
                    <span>Prodyna</span>
                </Link>
                <DismissButton onClick={drawerClickHandler} />
            </header>
            <ul className={styles.menu}>
                <li>
                    <NavLink
                        to={MAIN_URLS.POSTS}
                        onClick={drawerClickHandler}
                        className={({ isActive }) =>
                            isActive
                                ? styles.side_drawer__active_link
                                : styles.side_drawer__link
                        }
                    >
                        Posts
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        to={MAIN_URLS.CREATE_POST}
                        onClick={drawerClickHandler}
                        className={({ isActive }) =>
                            isActive
                                ? styles.side_drawer__active_link
                                : styles.side_drawer__link
                        }
                    >
                        Create Post
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default SideDrawer;
