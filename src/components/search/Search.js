import React from 'react';

import styles from './Search.module.scss';

const Search = ({ value, onChange, placeholder, noResults }) => {
    return (
        <>
            <input
                type="search"
                value={value}
                onChange={onChange}
                placeholder={placeholder}
                className={styles.search}
            />
            {noResults && (
                <div>No results for your query. Try different one?</div>
            )}
        </>
    );
};

export default Search;
