import React from 'react';

import styles from './Loader.module.scss';

const Loader = () => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.loader}>
                <div className={styles.loader__dot_1}></div>
                <div className={styles.loader__dot_2}></div>
            </div>
        </div>
    );
};

export default Loader;
