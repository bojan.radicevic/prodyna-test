import React, { memo } from 'react';

import defaultAvatar from 'assets/images/avatar.png';

import styles from './Avatar.module.scss';

const Avatar = ({ src }) => {
    return (
        <div className={styles.avatar}>
            <img src={src || defaultAvatar} alt="user avatar" />
        </div>
    );
};

export default memo(Avatar);
