import React, { useState } from 'react';
import { Link, Outlet, NavLink } from 'react-router-dom';

import SideDrawer from 'components/sideDrawer/SideDrawer';
import DrawerToggleButton from 'components/drawerToggleButton/DrawerToggleButton';

import logo from 'assets/images/prodyna_logo.png';

import styles from 'App.module.scss';
import { MAIN_URLS } from 'util/routes/urls';

function App() {
    const [showSideDrawer, setShowSideDrawer] = useState(false);

    const drawerToggleHandler = () => {
        setShowSideDrawer((prev) => !prev);
    };

    return (
        <>
            <SideDrawer
                drawerClickHandler={drawerToggleHandler}
                show={showSideDrawer}
            />
            <div className={styles.app}>
                <header className={styles.app__header}>
                    <nav>
                        <Link to={MAIN_URLS.POSTS}>
                            <img
                                src={logo}
                                className={styles.app__logo}
                                alt="logo"
                            />
                            <span>Prodyna</span>
                        </Link>
                        <DrawerToggleButton click={drawerToggleHandler} />
                        <ul className={styles.app__nav_menu}>
                            <li>
                                <NavLink
                                    to={MAIN_URLS.POSTS}
                                    className={({ isActive }) =>
                                        isActive
                                            ? styles.active_link
                                            : styles.link
                                    }
                                >
                                    Posts
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to={MAIN_URLS.CREATE_POST}
                                    className={({ isActive }) =>
                                        isActive
                                            ? styles.active_link
                                            : styles.link
                                    }
                                >
                                    Create Post
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </header>
                <main>
                    <Outlet />
                </main>
            </div>
        </>
    );
}

export default App;
