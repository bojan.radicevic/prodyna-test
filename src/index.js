import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import Router from 'app/Router';
import { store } from 'store/store';

import 'index.scss';

const app = (
    <StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <Router />
            </BrowserRouter>
        </Provider>
    </StrictMode>
);

ReactDOM.render(app, document.getElementById('root'));
